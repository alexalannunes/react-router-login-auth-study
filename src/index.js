import React from "react";
import ReactDOM from "react-dom";
import MApp from './App';
import { BrowserRouter as Router } from 'react-router-dom';

const App = () => (
  <Router>
    <MApp />
  </Router>
)

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
