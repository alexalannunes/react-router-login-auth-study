import React from "react";
import axios from 'axios';
import { Switch, Route, Redirect } from 'react-router-dom';
import Home from "./screens/Home";
import App from "./screens/App";
import Login from "./screens/Login";
import Auth from './Auth';

const auth = new Auth();


const PrivateRote = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      auth.is_logged() ? <Component {...props} /> : <Redirect to={{ pathname: '/login' }} />
    )}
  />
);

class MApp extends React.Component {
  render() {
    return (
      <Switch>
        <Route exact path='/login' component={Login} />
        <PrivateRote path='/app' component={App} />
        <PrivateRote path='/' component={Home} />
      </Switch>
    );
  }
}

export default MApp;