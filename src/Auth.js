class Auth {

  /**
   * salva
   * @param {Object} data 
   */
  save_data(data) {
    localStorage.setItem('@user', JSON.stringify(data));
  }

  /**
   * get data
   */
  get_data() {
    return JSON.parse(localStorage.getItem('@user'));
  }

  is_logged() {
    return localStorage.getItem('@user') ? true : false;
  }
}

export default Auth;