import React, { Component } from 'react';
import axios from 'axios';
import Auth from '../Auth';

class Login extends Component {
  login = () => {
    axios.get('http://localhost:8000').then(
      response => {
        new Auth().save_data(response.data);

        setTimeout(() => {
          window.top.location.href = '/';
        }, 1000)
      }
    )
  }
  render() {
    return (
      <div>
        <button onClick={this.login}>Login</button>
      </div>
    );
  }
}

export default Login;