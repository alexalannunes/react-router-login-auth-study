import React, { Component } from 'react';
import Auth from '../Auth';

class Home extends Component {
  render() {
    return (
      <div>
        {JSON.stringify(new Auth().get_data())}
      </div>
    );
  }
}

export default Home;