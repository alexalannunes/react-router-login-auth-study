<?php

  ini_set('display_errors',1);
  ini_set('display_startup_erros',1);
  error_reporting(E_ALL);
  
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  class Auth {
    private $ua;
    private $patter;

    public function __construct()
    {
      $this->ua = $_SERVER['HTTP_USER_AGENT'];
      $this->patter = '/' . 'Chrome' . '/';
    }

    public function valid_user()
    {
      if (preg_match($this->patter, $this->ua))
      {
        return true;
      }
      else
      {
        return false;
      }
    }

    public function gen_oken()
    {
      return base64_encode(md5(time())) . '_A';
    }

    public function init_cookie()
    {
      if ($this->valid_user())
      {
        $arr = [
          "id" => sha1(time()),
          "username" => "alexalannunes",
          "permissions" => ["all", "admin", "master"],
          "token" => $this->gen_oken()
        ];
  
        return (object) $arr;
      }
      else
      {
        $err = ["error" => "erro_valid_user"];

        return (object) $err;
      }
    }

    public function auth()
    {
      return $this->init_cookie();
    }
  }

  

  $a = new Auth();

  $b = $a->auth();

  echo json_encode($b);
